// GraphFunction.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>

using namespace std;

int main()
{

	int valueX;
	int valueY;

	cout << "Enter the argument: ";
	cin >> valueX;

	if(valueX<-2 || valueX >2)
	{
		valueY=0;
	}

	else if(valueX >= -2 && valueX <=-1)
	{
		valueY = -valueX - 2;
	}

	else if (valueX >= -1 && valueX <=1)
	{
		valueY = valueX;
	}
	else if (valueX >= 1 && valueX <=2)
	{
		valueY = -valueX + 2;
	}

	cout << "Result: " << valueY <<endl;
	system("pause");


    return 0;
}

